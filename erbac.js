(function ($, Drupal) {

  /** ********************************************************************
   * INIT
   ** ***************************************************************** */

  Drupal.erbac = Drupal.erbac || {}
  Drupal.settings.erbac = Drupal.settings.erbac || {}


  /** ********************************************************************
   * BEHAVIORS
   ** ***************************************************************** */

  Drupal.behaviors.erbac = {
    attach: function(context, settings) {

      $('.field-widget-entityreference-erbac', context).once('erbac', function () {
        $(this).each(function(){
          // Get both fields and entity links list
          var erbacDefaultField = $(this).find('input[data-erbac-default-field]'),
              erbacPseudoField = $(this).find('input[data-erbac-pseudo-field]'),
              erbacEntityListWrapper = $(this).find('.erbac-entity-list-wrapper')

          // Delete default value in pseudo field
          erbacPseudoField[0].value = '';

          // Bind pseudo field submit
          Drupal.erbac.pseudoFieldSubmit($(this), erbacEntityListWrapper, erbacDefaultField, erbacPseudoField);

          // Manage entity list
          Drupal.erbac.entityList($(this), erbacEntityListWrapper, erbacDefaultField, erbacPseudoField);

        })
      });
      
    }
  } // Drupal.behaviors.erbac



  /** ********************************************************************
   * FUNCTIONS
   ** ***************************************************************** */

  /**
   * Theme function for delete button
   */
  Drupal.theme.prototype.erbacDeleteButton = function(entityId) {
    // data attribute 'data-delete-entity' is mandatory
    return '<a href="#" data-delete-entity="'+ entityId +'">'+ Drupal.t("Delete") +'</a>';
  }

  Drupal.erbac.pseudoFieldSubmit = function(wrapper, entityListWrapper, defaultField, pseudoField){

    // Bind pseudo field change 
    pseudoField.bind('autocompleteSelect', function(){

      // Add new link to entity list
      Drupal.erbac.addEntity(entityListWrapper, defaultField, pseudoField);

      // Clear pseudo field value
      pseudoField[0].value = '';

    })

  }

  Drupal.erbac.entityList = function(wrapper, entityListWrapper, defaultField, pseudoField) {
    // Make sure we have an entity list.
    if (entityListWrapper.find('.erbac-entity-list').length == 0) {
      $('<ul class="erbac-entity-list"></ul>')
        .appendTo(entityListWrapper.find('div'))
    }

    // Add delete button to every entity link
    $('[data-entity-id]', entityListWrapper).each(function(){
      Drupal.erbac.addDeleteButton($(this))
    })

    // Bind click on 'remove' entity link.
    $('[data-delete-entity]', entityListWrapper).live('click', function(e){
      e.preventDefault();

      // Remove entity
      Drupal.erbac.removeEntity(defaultField, $(this).siblings());

      // Allow use of #ajax custom event 'erbacChange' to be fired.
      // We'd like to fire change event, but it fails when using this widget
      // in an ajax form, because browsers handle events in different orders
      // (see http://bugs.jqueryui.com/ticket/9453)
      pseudoField.trigger('erbacChange');
      
    })
    
  }

  // Append delete button to entity link
  Drupal.erbac.addDeleteButton = function(entityLink) {
    entityLink.after(Drupal.theme('erbacDeleteButton', entityLink.data('entity-id')));
  }

  // Add an entity (link and field value)
  Drupal.erbac.addEntity = function(entityListWrapper, defaultField, pseudoField) {
    
    // Get entity from value
    var entity = Drupal.erbac.getEntityFromString(pseudoField[0].value);
    if (entity) {

      // Get entity list from field value
      var entityList = Drupal.erbac.explodeEntityList(defaultField[0].value);

      // Check if entity is not already present
      if (!entityList['id'+entity.id]) {

        // Create ajax url to retrieve entity link.
        var ajaxUrl = Drupal.settings.basePath + 'erbac/entity-link';
        ajaxUrl += '/' + pseudoField.data('erbac-field-name');
        ajaxUrl += '/' + pseudoField.data('erbac-entity-type');
        ajaxUrl += '/' + pseudoField.data('erbac-entity-bundle');
        ajaxUrl += '/' + entity.id;

        $.ajax({
          url: ajaxUrl,
          success: function (response, status) {
            // Update entity object
            entity = response.entity;

            // Add entity to list
            entityList['id'+entity.id] = entity;
            // Update default field value
            defaultField[0].value = Drupal.erbac.implodeEntityList(entityList);

            // Allow use of #ajax custom event 'erbacChange' to be fired.
            // We'd like to fire change event, but it fails when using this widget
            // in an ajax form, because browsers handle events in different orders
            // (see http://bugs.jqueryui.com/ticket/9453)
            pseudoField.trigger('erbacChange');


            // Add entity link
            $(response.htmlLink)
              .wrap('<li></li>')
              .parent()
              .appendTo(entityListWrapper.find('.erbac-entity-list'))

            // Add delete buttton
            Drupal.erbac.addDeleteButton($('[data-entity-id='+entity.id+']', entityListWrapper))

          },
          complete: function (response, status) {
            //~ ajax.ajaxing = false;
            if (status == 'error' || status == 'parsererror') {
              //~ return ajax.error(response, ajax.url);
            }
          },
        });

      }

    }
    
  }

  // Remove entity (link and value)
  Drupal.erbac.removeEntity = function(defaultField, entityLink) {
    
    // Remove entity from list
    var entityList = Drupal.erbac.explodeEntityList(defaultField[0].value);
    delete entityList['id'+entityLink.attr('data-entity-id')];
    defaultField[0].value = Drupal.erbac.implodeEntityList(entityList);

    // Remove link
    entityLink
      .parent()
      .remove();
  }



  /** ********************************************************************
   * HELPERS
   ** ***************************************************************** */

  // Return array
  Drupal.erbac.explodeEntityList = function(entityList) {
    var entityListArray = {};
    if (entityList.length) {      
      $.each(entityList.split(', '), function(k, v) {
        var entity = Drupal.erbac.getEntityFromString(v);
        if (entity) {
          entityListArray['id'+entity.id] = entity;
        }
      });
    }
    return entityListArray;
  }

  // Return string
  Drupal.erbac.implodeEntityList = function(entityListArray) {
    var entityList = [];
    $.each(entityListArray, function (k, v) {
      entityList.push(v.label+' ('+v.id+')');
    });
    return entityList.join(', ');
  }

  Drupal.erbac.getEntityFromString = function(entityString) {
    var tmp = /.+\((\d+)\)/g.exec(entityString);
    if ($.isArray(tmp)) {
      var entity = {};
      entity.id = tmp[1];
      entity.label = entityString.replace(' ('+entity.id+')', '');
      return entity;
    }
  }

  /** ********************************************************************
   * OVERRIDES
   ** ***************************************************************** */

  /**
   * Override default Drupal autocomplete.js hidePopup function.
   * Trigger a custom 'autocompleteSelect' event
   */
  Drupal.jsAC.prototype.hidePopup = function (keycode) {
    // Select item if the right key or mousebutton was pressed.
    if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
      this.input.value = $(this.selected).data('autocompleteValue');
      // Custom: add an event trigger
      $(this.input).trigger('autocompleteSelect');
    }
    // Hide popup.
    var popup = this.popup;
    if (popup) {
      this.popup = null;
      $(popup).fadeOut('fast', function () { $(popup).remove(); });
    }
    this.selected = false;
    $(this.ariaLive).empty();
  };


})(jQuery, Drupal);
